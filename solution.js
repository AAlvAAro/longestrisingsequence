Array.prototype.last = function() {
	return this[this.length - 1];
}

function longestRisingSequenceFinder(arr) {
	let subSequences = [];
  let subSequence = [];

  for (let i = 0; i < arr.length; i++) {
  	let number = arr[i];

  	if (subSequence.length > 0 && i === arr.length - 1 && subSequence.last() < number) {
    	subSequence.push(number);
      subSequences.push(subSequence);
    } else if (subSequence.lenght === 0) {
    	subSequence.push(number);
    } else if (subSequence.last() < number) {
    	subSequence.push(number);
    } else {
    	subSequences.push(subSequence);
      subSequence = [number]
    }
  }

  return subSequences.sort(function(a, b) {
  	return a.length - b.length;
  }).last();
}

module.exports = { longestRisingSequenceFinder }
