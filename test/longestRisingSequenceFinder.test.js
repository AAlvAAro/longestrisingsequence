const assert = require('assert');
const expect = require('chai').expect;
const solution = require('../solution');

describe('longestRisingSequenceFinder', function() {
  it('should return the longest rising sub sequence from an initial sequence', function() {
    expect([-3, 3, 7, 9]).to.eql(solution.longestRisingSequenceFinder([4, 6, -3, 3, 7, 9]));

    expect([4, 5]).to.eql(solution.longestRisingSequenceFinder([9, 6, 4, 5, 2, 0]));

    expect([0]).to.eql(solution.longestRisingSequenceFinder([0, 0, 0, 0]));

    expect([-2, -1, 0]).to.eql(solution.longestRisingSequenceFinder([1, 2, -2, -1, 0, -2, 1, 0]));
  });
});
